<?php get_header(); ?>
<div class="container">
  
  <h1 class="titre text-center mt-5">ATELIERS-CONFÉRENCES</h1>

  <h2 class="text-center mt-4 mb-5">Pour découvrir en expérimentant</h2>

  <h4 class="m-0 font-weight-bold">Quoi de mieux que l’expérimentation ?</h4>

  <p class="mt-3 mb-5">La parcours de Sophro-Analyse est individuel et se prête mal à une expérimentation collective.
    Pour vous permettre de découvrir la méthode, j’organise tous les deux mois des ateliers-conférences en petits groupes
    (4 à 5 personnes) où, à travers quelques exercices en état de relaxation, vous avez la possibilité d’éprouver de
    l’intérieur ce à quoi ressemble le travail effectué en séance. </p>

  <img class="mb-5" src="<?php echo get_stylesheet_directory_uri(); ?>/image/LDD theatre-2.jpg" alt="" width="100%">

  <h4 class="m-0 font-weight-bold">Le programme de ces ateliers de 2 h :</h4>
  <p class="mt-3 mb-2"><b>- Accueil</b> décontracté autour d’un thé</p>
  <p class="mt-3 mb-2"><b>- Partie Conférence : présentation orale</b> des enjeux et du fonctionnement de la Sophro-Analyse des mémoires prénatales</p>
  <p class="mt-3 mb-2"><b>- Partie Atelier : expérimentation</b> avec deux exercices suivi chacun d’un feedback/partage pour ceux qui souhaitent s’exprimer.</p>

  <p class="mt-5 mb-5">Les places étant limitées, votre inscription est obligatoire. Pour vous inscrire à Paris, cliquez <a href="<?php the_field('contact'); ?>">ici</a>. Pour vous inscrire à Saint-Mammès, cliquez <a href="<?php the_field('contact'); ?>">ici</a>.
    <br><br> Participation de 15 €.
    <br><br><br>
    Prochaines dates : cliquez <?php
                                $link = get_field('prochaines_dates');
                                if ($link) :
                                  $link_url = $link['url'];
                                  $link_title = $link['title'];
                                  $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
      <a class="blog" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">ici</a>
      <?php endif; ?></a>.</p>


</div>
<?php get_footer(); ?>