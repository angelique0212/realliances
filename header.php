<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Document</title>
    <?php wp_head(); ?>
</head>

<body>
        <div class="logo" width="50%">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/logo-realliance.png" alt="" width="30%" style="margin-left:50px" ;>
        </div>

    <nav class="nav">
       
        <?php wp_nav_menu([
            "theme_location" => "nav_menu"
        ]); ?>
    </nav>
    <div class="container">
    <div id="ancre">
        <a href="#"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path id="couleurchevron" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z" />
            </svg></a>
    </div>
    </div>