<?php get_header(); ?>


<div class="container mt-5 mb-5">

    <div class="row">

        <div class="col-12 mt-5">

            <h1 class="titre text-center mb-5">PRENDRE RENDEZ-VOUS</h1>

            <div class="justify-content-center">

                <p>Pour prendre rendez-vous, vous pouvez m’adresser un message en remplissant le formulaire de contact
                    <a href="<?php the_field('contact'); ?> "> ICI</a>
                </p>

                <p>ou m’appeler sur mon mobile, au <a class=»listButton » href="tel:+33616775494"> 06.16.77.54.94 </a> </p>
                <p><b>Pour permettre à ceux qui sont loin, malades ou en confinement d’accéder aux séances de Sophro-Analyse, j’ai ouvert un compte Skype dédié.</b></p>

                <p style="color:rgba(232,11,0,1);">Des facilités de règlement sont proposées aux demandeurs d’emploi  et aux étudiants.</p> 
                <p><b>Danièle Marin
                        Praticienne certifiée en Sophro-Analyse des mémoires prénatales,
                        Constellations familiales et systémiques en cabinet, Coaching.</b></p>

                <p><b>Adresse : Paris Centre</b>, 5 mn du métro Châtelet<br>
                    <b> (Reçoit aussi dans la région de Fontainebleau)<br>
                        [realliances@free.fr]</b></p>

            </div>
        </div>
    </div>

    <div class="col-lg-12 col-xl-6 mt-5">
            
            <p class="mt-5 text-justify"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/arc-en-ciel.jpg" style="" alt="" width="50%" height="80%">    
            </p>
            <p> Gay Friendly</p>
        </div>
        <div class="row mt-5 mb-5">
  
  
</div>

</div>
<?php get_footer(); ?>