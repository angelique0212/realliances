<?php get_header(); ?>
<style>
    .blog {
        text-align: center;
    }
</style>
<div class="blog mb-5 mt-5">
    <p><?php the_content(); ?></p>
</div>
<div class="container">
    <div class="row mb-5">
        <div class="col-lg-12 col-xl-10">
            <div style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/seagull.jpg);background-repeat:no-repeat;width:100%;background-size:cover">

            </div>

        </div>
    </div>
    <div class="row mt-5 mb-5 text-center">

        <div class="col-lg-12 col-xl-12">
            <h3 class="titre text-center">De l’insécurité à la joie</h3>
            <p class="mt-4 text-justify"><b>Annick de Souzenelle</b>, thérapeute et écrivaine d’inspiration jungienne, se consacre au décryptage
             des symboles (Le Symbolisme du corps humain, 2000) et enseignements à l’œuvre dans les textes sacrés des grandes traditions mondiales.
             
            </p>
            <p class="text-justify">Au détour d’un entretien sur les déserts que chacun de nous est appelé à traverser au cours de sa vie (moments de « désécurisation » matérielle ou affective, voire spirituelle ou intellectuelle), elle prononce ces paroles, riches de promesses, qui sont comme l’écho d’un parcours réussi en Sophro-Analyse : </p>
         
            <p class="text-justify"> <b>« Nous avons tous un retournement à faire pour ne plus attendre la sécurité du monde extérieur mais l’acquérir du monde intérieur.
                    <br>[…]<br>
                    Qui ne l’a pas fait ne peut pas se rendre compte. Mais en même temps c’est, quand on a eu la grâce de pouvoir le faire, la joie incroyable… la joie incroyable et qui ne vous quitte plus !... » </b></p>
            <p class="text-justify">Accompli et non encore accompli,<a href="https://www.youtube.com/watch?v=DRIgz18vpa0"> https://www.youtube.com/watch?v=DRIgz18vpa0</a></p>
        </div>

    </div>


        <div class="row mt-5 mb-5">
            <div class="col-lg-12 col-xl-6 mt-5">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/livre.jpg" alt="" width="50%">
            </div>
            <div class="col-lg-12 col-xl-6 mt-5">
                <h4 class="titre text-center">Pour en apprendre plus sur la Sophro-Analyse :</h4>
                <p class="mt-5 text-justify">Un livre passionnant : Christine Louveau, La Sophro-analyse des mémoires prénatales , de la naissance et de l’enfance. Votre âme aux commandes, Paris, éditions Grancher, 2017, 592 pages.
                </p>
               
                    <p class="text-justify">La vidéo d’une conférence donnée par Christine Louveau au salon Zen en 2014 :<a href="https://www.youtube.com/watch?time_continue=24&v=-kVBvjt0EkU&feature=emb_logo"> https://www.youtube.com/watch?time_continue=24&v=-kVBvjt0EkU&feature=emb_logo</a></p>
                </p>

                <div class="col-lg-12 col-xl-6 mt-5">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/imagevideo.png" alt="" width="210%">
                </div>


    

    </div>


</div>

</div>
<?php get_footer(); ?>