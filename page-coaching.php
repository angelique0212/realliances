<?php get_header(); ?>
<div class="container">

   
    <div class="col-12">
    <h1 class="titre text-center mt-5 ml-5">Coaching de Vie</h1><br>
        <div class="ml-n3" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/seagull.jpg);background-repeat:no-repeat;width:100%;height:650px;background-size:cover">
            <h3 class="opacite" style="font-style: italic;font-size: 2.3rem;letter-spacing: 0.141em;" ;><br><br><br><br><br>
                «Le succès n’est pas la clé du bonheur. Le bonheur est la clé du succès. <br>(Albert Schweitzer)</h3>
        </div>

            <div class="row mt-5 mb-5">
                 <div class="mt-5 text-center">
                    <h4 class="titre text-center">Accompagner éthiquement la personne jusqu’à la réalisation concrète de son objectif</h4>
                    <p class="mt-4 text-justify">Baptisé<b> Coaching de l’Excellence®</b> par ses créateurs, le coaching auquel j’ai été formée va au-delà d’un accompagnement ciblé sur un domaine
                        spécifique de l’existence et s’adresse à l’ensemble de la personne, dans tous les aspects de sa vie.
                    </p>
                    <p class="text-justify">Ce coaching fait appel aux plans physique, émotionnel, mental et spirituel de la personne afin de lui permettre de découvrir en elle les ressources, souvent ignorées ou méconnues, sur lesquelles s’appuyer pour approfondir son projet de vie, identifier et lever
                        les éventuels obstacles à son accomplissement et mettre en œuvre concrètement son plan d’action.</p>
                    <p class="text-justify">Pragmatique, créatif, ce coaching est un coaching du cœur. Il s’ouvre à l’intuition et se montre respectueux de l’écologie de la personne qu’il a pour vocation de révéler à elle-même afin que, passant de l’idée de contrainte à la réalisation de soi,
                        elle aille librement vers le sens à donner sa vie. </p>
                    <p class="text-justify">Le coaching constitue un formidable tremplin pour la modification des
                        comportements. Puissant révélateur et remarquable outil diagnostique pour la personne elle-même, il entre en résonance avec l’approche thérapeutique,
                        qu’il complète admirablement en tant que booster de la mise en action. </p>

                </div>

            </div>

            <div class="row mt-5 mb-5">

                <div class="col-lg-12 col-xl-4 ml-n3">
                    <img class="barriere" src="<?php echo get_stylesheet_directory_uri(); ?>/image/summer-holiday-1510556_1920.jpg"  alt="">
                </div>

                <div class="mt-5 text-center">
                    <h4 class="titre text-center">Comment se déroule une séance</h4>
                    <p class="mt-4 text-justify">
                       <b> Le Coaching de l’Excellence®</b> se fonde sur des outils ludiques conçus pour que s’expriment pleinement les ressources créatives de notre Enfant intérieur. Chaque séance s’appuie sur un exercice
                        unique qui approfondit votre connaissance de vous-même et modifie votre regard sur la vie.
                    </p>
                    <p class="text-justify">A l’issue de cet exercice, vous êtes appelé à exprimer les prises de conscience
                        qui ont éclos en vous, choisir une action concrète en rapport avec ce changement de conscience et
                        l’objectif global défini.
                        lors de la première séance, déterminer quand et à quelle fréquence vous vous engagez à réaliser votre action.</p>

                </div>

            </div>
    </div>
</div>
<?php get_footer(); ?>