<?php get_header(); ?>

<style type="text/css">
    #banniere {
        height: 300px;
        width: 100%;
    }
</style>

<div class="container">

    <h1 class="titre text-center mt-5 ml-5">Constellations Familiales et Systémiques individuelles </h1>

    <div>

        <h5 style="color:#03bbf7" class="text-justify mt-5 ml-n2"> « <b class="font-italic">La vie n’a pas de sens.</b>
            Ni sens interdit, ni sens obligatoire.
            Et si elle n’a pas de sens, c’est qu’elle va dans tous les sens et déborde de sens, inonde tout.
            Elle fait mal aussi longtemps qu’on veut lui imposer un sens, la tordre dans une direction ou une autre.
            Et si elle n’a pas de sens, c’est qu’<b class="font-italic">elle est le sens.</b> » (Christiane Singer )</h5>
    </div>

    <div class="row mt-5 mb-5">
        <div class="col-lg-12 col-xl-6 mt-5">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/lego.jpg" style="margin-top: 70px;transform: rotate(350deg);" alt="" width="100%">
        </div>
        <div class="col-lg-12 col-xl-6 mt-5">
            <h4 class="titre text-center">Présentation</h4>
            <p class="mt-5 text-justify">Lointaines héritières des modes de résolution des conflits en usage dans les sociétés primitives, les constellations familiales sont une <b style="color: rgba(232,11,0,1);">approche</b> systémique <b style="color: rgba(232,11,0,1);">transgénérationnelle </b>théorisée par le psychothérapeute allemand Bert Hellinger dans les années 1990.
            </p>
            <p class="text-justify">Elles ont pour vocation de mettre au jour les « lois » problématiques inconscientes régissant la place de chacun au sein d’une famille particulière observée comme système et d’en réparer les perturbations, à l’origine des troubles ressentis par le consultant.
            </p>
            <p class="text-justify">En séance individuelle,<b style="color: rgba(232,11,0,1);"> ce sont des figurines qui, choisies intuitivement par le client, représentent les protagonistes du problème à résoudre.</b> </p>
        </div>

    </div>


    <div class="row mt-5 mb-5">
  
<div class="col-lg-12 col-xl-6"><br>
<h2 class="titre text-center mt-5">Guérir la famille et se libérer</h2><br>
<h4 class="text-center"style="color:rgba(232,11,0,1);">Histoires personnelle et transgénérationnelle se conjuguent. </h4><br>
    <p class="mt-4 text-justyfy">
       <p> Le recours aux Constellations Familiales et Systémiques
                        s’impose souvent au cours du processus sophro-analytique, lorsque la problématique
                        rencontrée par la personne implique la famille ou la généalogie.<p>
                            <p>Les drames, deuils mal assumés, secrets et non-dits de votre généalogie
                                vous ont été transmis en même temps que la vie, et influencent votre existence à votre insu. </p>
                            <p style="color: rgba(232,11,0,1);">Vous êtes ainsi l’héritier innocent de problèmes que vous portez souvent par
                                loyauté, enchaîné (« intriqué ») à un personnage de votre arbre qui réclame réparation et pour
                                le compte duquel vous agissez sans le savoir…</p>
                            <p>…jusqu’à ce que, par votre travail en constellations, vous rameniez l’équilibre
                                et la justesse dans le système et l’inconscient familiaux. En dénouant ces intrications,
                                vous vous libérez (vous et vos descendants) de ces « dettes » contractées
                                dans l’ignorance pour sortir des schémas empruntés, suivre votre propre chemin et vivre enfin votre vie. </p>
                            <p>Les figurines que vous choisissez sur un mode intuitif vous permettrons de visualiser les déséquilibres de votre lignée en écho avec vos problématiques actuelles et vous pourrez, au terme d’un processus de restructuration tout en amour,
                                compassion et ouverture, accepter avec une infinie gratitude l’héritage sain de votre famille.</p>
                            <p style="color: rgba(232,11,0,1);">Une approche singulière qui ne tarde pas à manifester ses bienfaits sur le parcours d’évolution du « constellé ».
                            </p>

    </p>
</div>
<div class="col-lg-12 col-xl-6">
    <p class="mt-5 text-justify"><br>
    <img style="" src="<?php echo get_stylesheet_directory_uri(); ?>/image/pregnant-woman-1130611_1920.jpg" alt="femme enceinte" width="100%" height="800px">
    </p>
</div>

</div>
   
</div>
</div>
<?php get_footer(); ?>