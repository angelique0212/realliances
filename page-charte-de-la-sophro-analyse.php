<?php get_header(); ?>


<div class="container">

<h1 class="text-center font-weight-bold mt-5">CHARTE DE LA SOPHRO-ANALYSE DES MEMOIRES PRENATALES,DE LA NAISSANCE ET DE L’ENFANCE</h1>
<div class="row mt-5 mb-5">
    <div class="col-2">
        <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="">
    </div>
<div class="col-8">
    <h2 style="font-style: italic;">1  PREAMBULE</h2><br>
    <h3>1.1  Définition de la Sophro-analyse</h3><br>
    <p>La « Sophro-analyse des mémoires prénatales, de la naissance et de l’enfance » est issue de la Méthode du docteur Claude Imbert.
        Elle a été développée, enrichie, et théorisée à sa suite par Christine Louveau avec sa spécificité de « Votre âme aux commandes ©».
        C’est une méthode thérapeutique de transformation des croyances et des stratégies limitantes à l’origine des souffrances et des scénarios répétitifs de mal-être.
        Elle est orientée principalement vers la libération des mémoires et des empreintes du passé dont l’origine est inconsciente et qui conditionnent le fonctionnement et le vécu des individus.
        La Sophro-analyse est fondée sur les connaissances approfondies du fonctionnement psycho-émotionnel de l’être humain et s’appuie sur plus de 20 ans de découvertes et d’expériences pratiques.
        La dimension transpersonnelle apportée par Christine Louveau en fait une méthode psychothérapeutique complète qui dépasse le cadre thérapeutique classique et constitue un tremplin expérient­iel vers l’évolution spirituelle.</p>
    <br>
    <h3>1.2  Terminologie</h3><br>
    <p>Par souci de légèreté, le terme de « Sophro-analyse » utilisé dans ce document fait référence à la Sophro-analyse des mémoires prénatales, de la naissance et de l’enfance. Le terme de « Sophro-analyste »
        regroupe les Praticiens de Sophro-analyse (ayant obtenu leur Certification) ainsi que les Sophro-analystes (ayant complété leur Certification par l’obtention d’un diplôme après validation d’un mémoire professionnel).</p>
    <br>

    <h2 style="font-style: italic;">2  LES OBJECTIFS DE LA CHARTE</h2><br>
    <h3>2.1  Visibilité des Sophro-analystes</h3><br>
    <p>Une liste des Sophro-analystes est réactualisée au fil des nouvelles certifications. Cette liste est donc accessible au grand public via le site internet.
        L’adhésion à la Charte et l’inscription à l’annuaire des Sophro-analystes sur le site internet du Centre Emergence Harmonique  garantissent que ces praticiens se sont engagés à pratiquer dans l’esprit, la rigueur, et la déontologie de la méthode.
        Le Centre s’applique à :
        <ul>Communiquer les coordonnées des Sophro-analystes.</ul>
        <ul>Recommander les Sophro-analystes pour leur pratique conforme à l’éthique appliquée dans le cadre des études.</ul>
        <ul>Faire connaître auprès du public cette méthode thérapeutique.</ul>
    </p>
    <br>

    <h3>2.2  Eviter les dérives</h3><br>
    <h4>2.2.1  Respect de la méthode « Votre âme aux commandes © »</h4><br>
    <p>Afin d’éviter les dérives, les sophro-analystes inscrits qui ont signé cette charte s’engagent à respecter l
        a méthode mise au point par Christine Louveau et sa déontologie.
        <ul> Laisser l’âme aux commandes et garantir ainsi le libre-arbitre du client.</ul>
        <ul> Accompagner le client où que son âme le conduise.</ul>
        <ul> Installer et travailler dans un cadre de sécurité.</ul>
        <ul> Respecter la méthodologie de la séance enseignée par Christine Louveau au Centre d’Emergence Harmonique :</ul>
        <ul> Décoder et faire émerger les croyances, émotions et stratégies de l’empreinte retrouvée.</ul>
        <ul> Amener le client à prendre conscience des échos dans sa vie actuelle.</ul>
        <ul> Restructurer les empreintes avec les outils enseignés durant la formation.</ul>
        <ul> Faire émerger le sens de ce qui a été vécu durant la séance.</ul>
        <ul> Imprimer et installer les nouvelles décisions et les ressources découvertes jusque dans l’adulte.</ul>
        Le cadre de cette Charte garantit aux clients que la pratique professionnelle des Sophro-analystes certifiés par le Centre Emergence Harmonique
        se fait dans le respect de l’éthique synthétisée dans le présent document.</ul>
        Leur Certification valide le fait qu’ils justifient des connaissances et de l’expérience apportées par l’enseignement suivi.</ul>
        L’acquisition des évolutions de la méthode est réalisée par la participation aux supervisions et aux formations continues proposées.</ul>  
    </p><br>

    <h3>2.3  Critères d’inscription et d’intégration</h3><br>
    <h4>2.3.1  Les six critères</h4><br>
    <p>Tout Praticien doit justifier pour cela de la Certification éditée par le Centre Emergence Harmonique.
        La certification de base repose sur la validation de six critères :
        <ul>1. Thérapie personnelle en Sophro-analyse menée à son terme (20h environ)</ul>
        <ul>2. Participation aux cycles de l’enseignement (168h)</ul>
        <ul>3. Travail personnel (intégration, entraînements et synthèse -500 heures environ)</ul>
        <ul>4. Contrôle des connaissances et mémoire personnel (pour le titre de Praticien en Sophro-analyse)</ul>
        <ul>5. Participation aux supervisions (56h minimum sur 3 ans)</ul>
        <ul>6. Signature de la Charte</ul>
    </p><br>

    <h4>2.3.2  Complément à partir des promotions 2015</h4><br>
    <p>Depuis 2015, un recueil des témoignages d’au moins cinq de leurs clients est nécessaire au Sophro-analyste pour démontrer l’efficacité et la rigueur de son travail thérapeutique.
        Le thérapeute qui demande à être porté sur la liste, accepte et s’engage à respecter les conditions stipulées dans cette Charte.
        Pour l’obtention du titre de Sophro-analyste, le Praticien doit remettre un Mémoire Professionnel écrit sur la base de sa pratique.</p><br>


    <h3>2.4  Engagement par rapport à la Charte des Sophro-analystes</h3><br>
    <p>Les Sophro-analystes qui la signent, s’engagent au respect de la Charte.
        Celle-ci intègre de fait le Code de déontologie de la Fédération Française de Psychothérapie (FF2P) dans les grands domaines de :
        <ul>Responsabilité,</ul>
        <ul>Compétence</ul>
        <ul>Valeurs morales et normes juridiques,</ul>
        <ul>Confidentialité,</ul>
        <ul>Protection du client,</ul>
        <ul>Relations professionnelles et Déclarations publiques.</ul>
        Les éléments de cette charte sont consultables sur le site de la FF2P.
        Pour adhérer à la Charte de la Sophro-analyse, il suffit de renvoyer la fiche d’inscription obtenue auprès du Centre Emergence Harmonique après l’avoir dûment remplie, datée et signée à n’importe quel moment de l’année car les réactualisations de l’annuaire sont réalisées régulièrement.
    </p><br>

    <h2 style="font-style: italic;">3  ENGAGEMENTS DES SOPHRO-ANALYSTES</h2><br>
    <h3>3.1  Auprès du Centre Emergence Harmonique</h3><br>
    <h4>3.1.1  Dénomination</h4><br>
    <p>Les Praticiens certifiés par l’examen de validation des connaissances théoriques et pratiques et adhérents à la Charte, mais n’ayant pas encore présenté leur mémoire personnel se nomment « Praticiens de Sophro-analyse des mémoires prénatales, de la naissance et de l’enfance ».
        Les Sophro-analystes certifiés par la validation de leur Mémoire Professionnel et adhérents à la Charte, peuvent se définir comme : « Sophro-analyste des mémoires prénatales, de la naissance et de l’enfance».
        Les étudiants n’ayant pas leur certification, n’ont pas l’autorisation d’utiliser les titres ci-dessus mentionnés.</p><br>


    <h3>3.1.2  La Sophro-analyse doit être au cœur de la pratique</h3><br>
    <p>Les Sophro-analystes s’engagent en signant cette Charte à faire de la Sophro-analyse leur méthode principale de travail.
        Ils s’engagent également à éviter d’utiliser dans leur pratique d’autres techniques thérapeutiques qui modifieraient de manière significative la méthode originale.
        Si par la suite une ou plusieurs approches thérapeutiques étaient utilisées de manière dominante et constante dans sa pratique, il se doit de le faire savoir au Centre en demandant d’être retiré de la liste. Si le thérapeute ne prévenait pas le Centre et que celui-ci en était informé, il se réserve le droit de radier le Sophro-analyste de l’annuaire.
        L’association de toute autre technique pouvant présenter un risque d’amalgame à une connotation ésotérique n’est pas autorisée. Cependant si cette technique complémentaire constitue une autre facette du praticien, celui-ci doit la distinguer explicitement dans son discours et dans sa pratique des séances de Sophro-analyse.
        Ces techniques complémentaires doivent faire l’objet de dépliants ou documents séparés,
        Le Centre prend en compte des méthodes déjà partiellement intégrées dans la Sophro-analyse et validées par une formation officielle
        (Sophrologie, PNL, Analyse Transactionnelle, Psychogénéalogie, Constellations Familiales, …)</p><br>

    <h3>3.1.3  Intégration de la méthode et de la formation</h3><br>
    <p>Des supervisions régulières sont obligatoires durant les deux premières années de pratique et fortement conseillées ensuite (une par an). Elles font parties du maintien de l’adhésion à la liste des Sophro-analystes diffusée au public.
        Au-delà des deux premières années, des réunions d’Intervision de Sophro-analystes certifiés peuvent remplacer les supervisions.</p><br>

    <h3>3.1.4  Déontologie et éthique</h3><br>
    <p>Le cadre de la formation a pour but de former des Sophro-analystes et des Praticiens de Sophro-analyse des mémoires prénatales, de la naissance et de l’enfance. L’enseignement et sa coordination sont uniquement de la responsabilité du Centre et de sa formatrice Christine Louveau.
        Le Sophro-analyste s’engage à se servir du matériel pédagogique remis ou des enregistrements des cours réalisés durant la formation, à titre strictement personnel. Il s’engage à ne pas diffuser les cours ni les reproduire. Tout prêt, location, vente, usage privé ou public, et cela sous toutes ses formes, étant strictement interdit par la loi du 11 Mars 1957.
        Toute information diffusée par le Sophro-analyste, quels qu’en soient les supports, devra éviter toute déclaration fausse ou trompeuse. Elle devra être faite dans une position de réserve sur la personnalité du Sophro-analyste, la nature de la pratique et les résultats escomptés.
        Toute évocation mensongère est interdite, telles :
        <ul>Promesses irréalistes de guérison.</ul>
        <ul>Déclarations pouvant prêter à équivoque avec un usage illégal de la Médecine, par des citations réservées à ce domaine et générant des confusions avec un traitement médical,
            des diagnostics, prescriptions, traitements amenant la guérison…en association aux noms de symptômes et/ou maladies.</ul>
        <ul>Déclarations visant à inciter les clients à arrêter leurs suivis médicaux ou à les éviter.</ul>
        <ul>Déclarations créant une attente fausse ou injustifiée de résultats ou risquant de la créer.</ul>
        <ul>Déclarations sensationnelles et exagérées, généralisations abusives.</ul>
        <ul>Déclarations impliquant des compétences hors du commun et références à des approches thérapeutiques laissant supposer une formation plus étendue qu’elle ne l’est en réalité.</ul>
        <ul>En ce qui concerne certaines spécificités des techniques utilisées en synergie dans la méthode (Analyse Transactionnelle, PNL, Psychogénéalogie), l’enseignement a transmis des
            bases nécessaires pour leur utilisation dans le cadre de la méthode de Sophro-Analyse.</ul>
        <ul>Si une compétence particulière a été validée par le praticien auprès d’organismes de ces trois formations spécifiques, il pourra alors y faire référence.</ul>
        <ul>Formation simplement entamée ou terminée mais non validée par la certification.</ul>
        <ul>Déclarations comparant la Sophro-analyse à d’autres méthodes ou services offerts par d’autres professionnels.</ul>
    </p><br>

    <h3>3.1.5  Non appartenance à une secte</h3><br>
    <p>Conscient du caractère essentiel de la liberté de pensées et d’actions de chacun et de l’objectif d’autonomie prioritaire dans l’accompagnement des clients,
        le Sophro-analyste déclare sur l’honneur n’appartenir à aucune secte quelle qu’elle soit. La connaissance confirmée d’une telle appartenance
        serait à l’origine d’un retrait immédiat de l’agrément et de la liste.</p><br>

    <h3>3.1.6  Communication</h3><br>
    <p>Le Sophro-analyste a obligation de fournir des informations exactes et objectives sur son parcours ainsi que sur la méthode « votre âme aux commandes© ».
        Dans les documents d’information sur la méthode :
        <ul>Les citations philosophiques ou littéraires doivent éviter la confusion avec des courants de pensées notamment sectaires. Il est de la responsabilité du Sophro-analyste qui les utilise de vérifier ce point.</ul>
        <ul>Aucun logo ou sigle cabalistique ne doit être utilisé sur les documents d’informations concernant la Sophro-analyse, pour éviter des erreurs d’interprétation et de prêter à confusion avec l’appartenance à un ordre médical ou paramédical, à une secte ou confrérie quelle qu’elle soit.</ul>
        <ul>Aucun texte informatif ou publicitaire, quels qu’en soient les supports, n’a le droit d’utiliser les noms et titres d’un Sophro-analyste sans son autorisation expresse et son accord écrit concernant son libellé.</ul>
        Le logo de la méthode « votre âme aux commandes© » doit être mentionné par les Sophro-analystes sur leur sites et documents de communication.
        La version « écran » est obtenue en cliquant sur le logo ci-dessous. Une version HD peut être obtenue sur demande au centre Emergence Harmonique.</ul>
    </p><br>


    <div class="col-6 ">
        <img class="bougie" src="<?php echo get_stylesheet_directory_uri(); ?>/image/Logo Âme aux commandes.png" alt="" width="70%">
    </div><br>

    <h3>3.2  Auprès des clients</h3><br>
    <h4>3.2.1  Les valeurs fondamentales de la pratique des Sophro-analystes</h4><br>
    <p>Le Sophro-analyste s’engage à mettre les Valeurs de cette Charte au premier plan de sa pratique professionnelle avec ses clients.
        Il s’engage à garder une conscience éclairée de leur importance pour lui et les autres, à détecter tout ce qui pourrait les limiter et mettre tous
        les moyens en œuvre pour les amplifier, sachant que tout est toujours perfectible.
        <ul>Clarté,</ul>
        <ul>Honnêteté avec soi-même et les autres,</ul>
        <ul>Générosité de cœur, Compassion,</ul>
        <ul>Tolérance,</ul>
        <ul>Discernement,</ul>
        <ul>Humilité dans la conscience de sa juste valeur,</ul>
        <ul>Conscience spirituelle,</ul>
        <ul>Partage de toutes ses connaissances pour susciter l’autonomie,</ul>
        <ul>Absence d’égocentrisme et de préoccupation étroite de son propre avancement,</ul>
        <ul>Perfectionnement d’une conduite éthique,</ul>
        <ul>Noblesse de ses intentions, de ses pensées et de ses actes, sagesse,</ul>
        <ul>Développement de la parole juste.</ul>
    </p><br>

    <h4>3.2.2  Liberté du client</h4><br>
    <p>En dehors de quelques séances au début de l’accompagnement qui assurent la sécurisation du client, la méthode se caractérise par « votre Ame aux commandes© ».
        Le thérapeute accompagne le client là où son âme le ramène dans son histoire. Le Sophro-analyste se met donc au service de l’âme du client qui seule connaît s
        on histoire et les étapes nécessaires pour en libérer les nœuds en sécurité car elle seule en connaît l’histoire et les étapes nécessaire à sa libération.</p><br>

    <h4>3.2.3  Respect de l’être humain</h4><br>
    <p>Le Sophro-analyste doit exercer de manière compétente et dans le respect de l’éthique. Il reconnaît la dignité de tout être humain,
        indépendamment de son statut physiologique, psychologique, sociologique, culturel, religieux ou économique.</p><br>

    <h4>3.2.4  Continuité des traitements médicaux</h4><br>
    <p>En cas de maladie du client dont le diagnostic a été établi par un médecin, le Sophro-analyste vérifie que le malade est conscient
        du caractère essentiel de son suivi médical et du bon suivi de son traitement.</p><br>

    <h4>3.2.5  Respect du cadre de la relation thérapeutique</h4><br>
    <p>Le Sophro-analyste respecte l’intégrité et les valeurs propres de ses clients dans le cadre du processus de changement.
        Le Sophro-analyste se doit d’établir et surveiller les limites de sa relation avec ses clients et les leur rendre explicites, ne devant pas les exploiter soit sur le plan financier, sexuel, émotionnel ou autre.
        Le Sophro-analyste s’interdit d’entamer ou de maintenir un programme d’accompagnement lorsqu’il pourrait être compromis par des interférences relationnelles (amicales, affectives, professionnelles ou autres…).
        Le Sophro-analyste s’engage à prendre les mesures nécessaires pour offrir un environnement professionnel adéquat dans son cabinet, en respectant les besoins de
        ses clients et en conformité avec l’image de la méthode. Son lieu de pratique doit préserver le secret professionnel et assurer un service sécuritaire et de qualité.</p><br>

    <h4>3.2.6  Respect des consignes</h4><br>
    <p>Après la séance d’anamnèse réalisant le bilan de la demande du client et les objectifs principaux, un contrat thérapeutique est proposé. Celui-ci tient compte des contre-indications explorées durant l’entretien et des mentions ci-dessous :
        </ul>Des demandes du client, du programme thérapeutique du praticien et des objectifs réalistes proposés.</ul>
        </ul>Des conditions d’annulation et d’arrêt.</ul>
        </ul>Des conditions financières (honoraires, mode de règlement, séances manquées…).</ul>
        </ul>Du secret professionnel.</ul>
    </p><br>

    <h4>3.2.7  Cadre légal de l’exercice</h4><br>
    <p>Le Sophro-analyste doit exercer son activité professionnelle dans le cadre d’une structure légale dont le statut est à mentionner sur ses documents d’information. Le Centre demande pour l’adhésion à l’annuaire des Sophro-analystes, de préciser le cadre légal dans lequel le praticien réalise son rôle d’accompagnement. Le Centre ne peut reconnaître et accréditer d’autres pratiques.
         </p><br>

    <h3>3.3  Auprès de leurs collègues</h3><br>
    <h4>3.3.1  Devoir de réserve</h4><br>
    <p>Le Sophro-analyste dans ses déclarations et écrits est tenu à la réserve par rapport à ses confrères et formateurs. Il s’abstient de toute forme de dénigrement.
        Tout manquement à la pratique remarqué par un collègue doit être adressé au Centre directement avec à l’appui quelques témoignages.</p><br>

    <h4>3.3.2  Détournement de clientèle</h4><br>
    <p>Le détournement de clientèle est interdit.</p><br>

    <h2 style="font-weight: bold;">ENGAGEMENT DU CENTRE VIS-A-VIS DES SOPHRO-ANALYSTES</h2><br>

    <h3 style="font-style:italic;">4.1  Rôle et action d’Emergence Harmonique</h3><br>
    <h4>4.1.1  Soutenir la pratique</h4><br>
    <p>Le Centre s’engage à soutenir la pratique thérapeutique des Sophro-analystes agréés et à agir en tant que centre d’information sur la Sophro-analyse des mémoires prénatales,
        de la naissance et de l’enfance par l’intermédiaire du site internet Emergence Harmonique.<br>
        Le Centre Emergence Harmonique a pour mission de préserver la pureté, l’originalité et l’intégrité de la Sophro-analyse et de proposer une liste des Sophro-analystes certifiés qui s’engagent à respecter les termes et les conditions établis dans cette Charte.
        Le Centre s’appliquera à :
        <ul>Communiquer les coordonnées des Sophro-analystes.</ul>
        <ul>Les recommander pour leur pratique conforme à l’éthique et à la formation continue.</ul>
        <ul>Poursuivre ses informations et actions pour faire connaître auprès du public cette méthode thérapeutique.</ul>
    </p><br>

    <h3>4.2  Formation et communication</h3><br>
    <h4>4.2.1  Pérennité et continuité</h4><br>
    <p>Le Centre s’engage à la validation, la protection et la continuité de la profession ainsi que l’évolution de ses recherches.
        <ul>Il assure la continuité de la transmission dans le cadre de la formation continue.</ul>
        <ul>Il permet l’homogénéité des pratiques par l’intégration des recherches et des nouvelles découvertes.</ul>
        <ul>Il offre la continuité de la méthode par l’actualisation de la transmission.</ul>
    </p><br>

    <h4>4.2.2  Communication et colloques</h4><br>
    <p>Le Centre s’engage à développer, concevoir, éditer et diffuser sur tous supports, une communication de qualité.<br>
        A réactualiser une liste des Sophro-analystes sur son site internet.</p><br>

    <h3>4.3  Responsabilités</h3><br>
    <h4>4.3.1  Manquements à la Charte</h4><br>
    <p>Les Sophro-analystes ou le Centre se doivent de confronter un collègue ou un stagiaire s’ils ont des raisons de penser, sur des témoignages directs de clients, confrères ou public, qu’il y a manquement aux règles déontologiques, et si celui-ci agit de façon non conforme à la présente Charte.
        Le Centre s’engage alors à communiquer avec le Sophro-analyste en question et prend une décision qui s’impose grâce l’ensemble des informations rassemblées.</p><br>


    <h4>4.3.2  Suspension de la liste</h4><br>
    <p>Le Centre se réserve le droit sans notification préalable de suspendre une inscription de la liste s’il pense qu’une partie de la Charte n’a pas été respectée.
        Il en informe le Sophro-analyste concerné.</p><br>

    <h4>4.3.3  Responsabilité pleine et entière des Sophro-analystes</h4><br>
    <p>Bien que la présente Charte donne des directives pour une pratique déontologique garante de l’inscription à la liste des Sophro-analystes, ceux-ci demeurent toujours responsables de leurs actes et décisions. Le Centre Emergence Harmonique décline toute responsabilité des conséquences des actions et des pratiques des Sophro-analystes.<br>
        Le présent document ou la Certification délivrée par le Centre ne constituent pas un partenariat juridique avec le Centre Emergence Harmonique. <br>
        Aucune forme juridique ne lie le Centre et les Sophro-analystes en exercice.
    </p>
    </div>

        <div class="col-2 mb-5 align-self-end">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="">
        </div>
        </div>
</div>

<?php get_footer()?>