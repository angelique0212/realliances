<?php get_header(); ?>
<style>
    #ancre{
    margin-left: 95%;
    margin-top: 550px;
}
</style>
<div id="ancre">
                    <a href="#"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path id="couleurchevron" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"/>
                        </svg></a>
                </div>
 <div class="container">
<h1 class="titre text-center mt-5 mb-5">Témoignages</h1>
        <div >
            <img class="dance" src="<?php echo get_stylesheet_directory_uri(); ?>/image/StreetDance.jpg" alt="" width="100%" height="400px">
       
            <h3 class=" titre text-center mt-5 mb-5"> La Parole est aux « Renaissants »  </h3>
    <div class="row mt-5 mb-5 p-2">
    
        <div class="col-12 align-self-center">
            <div id="carouselContent" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active text-center p-4">
                        <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Guilletsgris.jpg" width="60px" >  Accompagnée avec bienveillance et respect, sur ce cheminement intime par la thérapeute
                            Danièle Marin, les yeux fermés, je me suis laissée guider pour aller visiter des endroits
                            difficiles. Son énergie positive, sa douceur, son calme m’ont permis de lâcher prise en
                            toute confiance et sécurité. Un accompagnement où perspicacité, ouverture d’esprit et
                            écoute ont été présentes à chacune de mes séances et ont contribué à être ces éléments
                            déclencheurs pour m’amener à la reconnaissance et la pacification de mes peurs, de mes
                            résistances. Le voyage est aussi une rencontre humaine, belle et enrichissante,
                            une lumière que m’a offerte Danièle et qui m’accompagne, à présent, à chaque instant.<br><br>[…]</p>
                    </div>
                    <div class="carousel-item text-center p-4">

                        <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Guilletsgris.jpg" width="60px" >  Le chemin ne fait que commencer, je continue à avancer, vers plus de légèreté et de sérénité,
                            afin de m’ouvrir d’avantage à ma liberté intérieure et à faire confiance à qui je suis…
                            M'éloigner de l’esprit pour sentir dans mon cœur, la lumière qui s’éveille.<br><br>
                            Quintina Z. </p>
                    </div>

                    <div class="carousel-item text-center p-4">

                        <p> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Guilletsgris.jpg" width="60px" >  Aujourd'hui, grâce à ce travail, je me sens plus apaisée. J'ai vu un grand progrès
                            à plusieurs niveaux. D’abord, par rapport à la relation à mon père, décédé, que
                            je voyais d'un regard totalement différent avant la thérapie. J'ai réussi à le
                            "rencontrer" et à faire la paix avec lui. D'autre part, j'ai lâché pas mal au niveau
                            du mental et suis plus à l’écoute des sensations. <br><br>[…]</p>
                    </div>

                    <div class="carousel-item text-center p-4">

                        <p><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Guilletsgris.jpg" width="60px" >  Je ne me sens plus illégitime et j'assume avec plus de confiance mon rôle de mère.
                            D'un autre côté, je suis moins sur le qui-vive, à vouloir tout contrôler.
                            Enfin, je réagis avec plus de calme dans les situations conflictuelles.
                            Aujourd'hui, c'est souvent moi la médiatrice dans des conflits entre les gens,
                            notamment au travail !<br><br>Jeanne W. </p>
                    </div>

                    <div class="carousel-item text-center p-4">

                        <p> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Guilletsgris.jpg" width="60px" >  Aujourd'hui je suis plus consciente et plus authentique. J'ai appris à me protéger
                            contre les abus de toutes sortes et je m'en porte mieux, notamment au travail.
                            Je vous remercie, Danièle, pour le respect que vous m'avez toujours témoigné, même dans les moments où vous
                            deviez me "bousculer" un peu pour que j'arrive à voir ce que je ne voulais pas voir. Je suis vraiment contente
                            de vous avoir fait confiance, moi qui me défiais des psychothérapeutes en tous genres. Merci encore de m'avoir
                            permis d'être plus moi-même. Je sais désormais que quels que puissent être les problèmes que je rencontrerai, j'aurai en moi tout ce qu'il faut pour y faire face.
                            <br><br> Gaëlle H. </p>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    </div>

</div>

<?php get_footer(); ?>

