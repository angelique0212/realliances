<?php get_header(); ?>

<div class="container">

    <div class="row mb-5">
        <div class="col-2 mt-5">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="" width="100%">
        </div>
        <div class="col-lg-12 col-xl-8">
            <h1 class="contacter">ME CONTACTER</h1>
            <div class="d-flex justify-content-center" ;>
                <?php echo do_shortcode(' [contact-form-7 id="6" title="Formulaire de contact 1"] '); ?></div>
        </div>
        <div class="col-2 mb-5 align-self-end">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="" width="100%">
        </div>
    </div>

    <div class="row mt-5 mb-5">
        <div class="colonne-cabinet col-lg-12 col-xl-4 p-5">
            <h3 class="text-center"><i class="fas fa-house-user"></i> Le cabinet</h3>
            <p class="text-center">- Situé au centre de Paris (M°Châtelet)<br>- Reçoit aussi à Fontainebleau</p>
        </div>
        <div class="colonne-horaire col-lg-12 col-xl-4 p-5">
            <h3 class="text-center"><i class="far fa-clock"></i> Les horaires</h3>
            <p class="text-center">Du lundi au vendredi <br> 10h-13h et 14h-18h30</p>
        </div>
        <div class="colonne-contact col-lg-12 col-xl-4 p-5">
            <h3 class="text-center"><i class="fas fa-phone-square-alt"></i> Me contacter</h3>
            <p class="text-center">06 16 77 54 94<br>realliances@free.fr</p>
        </div>
    </div>

</div>

<div class="container-fluid mb-5">
    <div class="row">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10598.627791580215!2d2.811749191497447!3d48.38632586808946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47ef5e4a55c3e8f3%3A0x40b82c3688c4c80!2sSaint-Mamm%C3%A8s!5e0!3m2!1sfr!2sfr!4v1599220894359!5m2!1sfr!2sfr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>
<?php get_footer(); ?>