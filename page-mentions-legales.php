<?php get_header(); ?>
<style>
    #ancre {
        margin-left: 95%;
        margin-top: 550px;
    }
</style>
<div id="ancre">
    <a href="#"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path id="couleurchevron" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z" />
        </svg></a>
</div>
<div class="container">
    <h1 class="titre text-center mt-5">Mentions Légales</h1>
    <div class="row">
        <div class="col-2 mt-3">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="" width="100%">
        </div>
        <div class="col-8">
            <p>Conformément à l’article 6 de la loi du 21 juin 2004, pour la confiance dans l’économie numérique,
                nous vous informons que ce service de communication au public en ligne est édité par : <br>
                Danièle MARIN, profession libérale, SIRET 390 741 239 00052 - Adresse : 25 rue du Bourg Tibourg 75004 PARIS – Tel : +33 6 16 77 54 94 </p><br>

            ​
            <p>
                Le directeur de la publication est Mme Danièle MARIN.<br>
                Le responsable de la rédaction est Mme Danièle MARIN.<br>​
            </p>

            <p>25 rue du Bourg Tibourg</p><br>

            <p> 75004 PARIS </p><br>

            <p>Tel : +33 6 16 77 54 94. </p><br>

            <p>SIRET 390 741 239 00052 </p><br>

            <p>Le prestataire mentionné au 2 du I de l’article 6 de la loi précitée (hébergeur) est : </p><br>

            <h4>WORDPRESS </h4><br>



            <p>Des cookies sont utilisés sur ce site pour donner à Danièle MARIN des statistiques d’accès aux différentes pages du site, ainsi que pour se souvenir de paramètres ou d’options de navigation.
                Ils ne sont aucunement utilisés à des fins publicitaires. </p><br>

            <p>realliances@free.fr </p><br>

            <p>06 16 77 54 94 </p><br>
        </div>
        <div class="col-2 mb-5 align-self-end">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="" width="100%">
        </div>
    </div>
</div>
<?php get_footer(); ?>