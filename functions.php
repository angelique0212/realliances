<?php

if (!function_exists("addStyle")) {
    function addStyle()
    {

        wp_enqueue_style("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css");
        wp_enqueue_style("font-awesome","https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css");
        wp_enqueue_script("bootstrap", "https://code.jquery.com/jquery-3.4.1.slim.min.js");
        wp_enqueue_script("bootstrap", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js");
        wp_enqueue_script("bootstrap", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js");
        wp_enqueue_style("my-stylesheet", get_stylesheet_directory_uri() . '/style.css');
    }
}
add_action("wp_enqueue_scripts", "addStyle");

register_nav_menus( [
    "nav_menu"=>__('menu', 'enssop'),
    "footer_menu" => __('Menu de pied de page', 'enssop')
]);
   
// Ajout de l'affichage des images mises en avant des publication du site
add_theme_support('post-thumbnails');
?>

