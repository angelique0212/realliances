<?php get_header(); ?>
<style>
    #ancre {
        margin-left: 95%;
        margin-top: 550px;
    }
</style>
<div id="ancre">
    <a href="#"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path id="couleurchevron" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z" />
        </svg></a>
</div>
<div class="container">
    <h1 class="titre text-center mt-5">Politique de confidentialité</h1>
    <div class="row mt-5 mb-5">
        <div class="col-2">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="">
        </div>
        <div class="col-8">
            <h3>Préambule et définitions</h3>

            <p>Nous accordons une importance particulière à la confidentialité et à la sécurité de vos données. C’est pourquoi nous mettons tout en œuvre pour que nos processus techniques et opérationnels respectent vos droits relatifs à la protection de vos données personnelles.

                Conformément à la loi « Informatique et Libertés » n°78-17 du 6 janvier 1978, ainsi qu’au nouveau Règlement Général sur la Protection des Données (RGPD), vous disposez d’un droit d’accès, de modification, de rectification et de suppression des données qui vous concernent.

                La présente Politique de Confidentialité vise à vous donner toutes les informations relatives à la protection de vos données personnelles mise en place par Danièle MARIN.
            </p>
            ​
            <h3>Données personnelles</h3>

            <p>Les “données personnelles” désignent l’ensemble des données que nous sommes susceptibles d’enregistrer par le biais de nos divers formulaires et qui peuvent permettre de vous identifier.

                Concrètement, cela peut aller de la simple adresse e-mail ou de votre numéro de téléphone en passant par vos noms et prénoms ou encore à des données plus « sensibles » comme des informations sur votre activité ou encore sur votre entreprise.
            </p>

            <h3>Collecte et consentement</h3>

            <p>Aussi, lorsque vous remplissez l’un de nos formulaires, vous reconnaissez avoir pris connaissance de la présente Politique de Confidentialité et vous convenez et acceptez que Danièle MARIN puisse recueillir, traiter, stocker et/ou utiliser ces données personnelles, dans le respect des règles de confidentialité et de sécurité exposées ci-après.
            </p>
            ​
            <h3>Identité et responsable du traitement de vos données personnelles</h3>

            <p>L’ensemble des données personnelles sont collectées sur notre site par Danièle MARIN , inscrit en profession libérale,

                SIRET 390 741 239 00052 – 25 rue du Bourg Tibourg 75004 PARIS – Tel : +33 6 16 77 54 94.
            </p>

            <h3>Données collectées sur le site</h3>

            <p>Lorsque vous remplissez l’un de nos formulaires, les données suivantes peuvent être collectées et/ou enregistrées :

                <p>Pour une personne physique : <br>

                    Nom <br>

                    Prénom <br>

                    E-mail <br>

                    Numéro de téléphone <br>

                    Ville de résidence <br>

                    adresse(s) IP <br>
                </p>

            </p>
            Pour une entreprise : <br>

            Nom de la société <br>

            Adresse de la société <br>

            Site internet de la société <br>

            Numéro de téléphone de la société <br>

            Adresse e-mail de contact de la société <br>

            N° de TVA Intracommunautaire <br>

            N° SIRET <br>

            adresse(s) IP <br>
            </p>


            Certaines données peuvent également être collectées automatiquement du fait de vos actions sur le site (voir le paragraphe relatif aux cookies ci-dessous).

            Les données collectées ne peuvent en aucun cas inclure de données personnelles sensibles, du type identifiants gouvernementaux (comme les numéros de sécurité sociale, de permis de conduire, ou les numéros d’identification de contribuable), numéros complets de carte de crédit ou de compte bancaire personnel, dossiers médicaux ou informations relatives à des demandes de soins associées à des personnes.
            </p>
            ​
            <h3>Destinataire des données collectées sur le site</h3>

            <p>
                Les données personnelles vous concernant collectées sur le site www.RealliAnceS.com sont destinées à l’utilisation exclusive de Danièle MARIN.

                La base de données de Danièle MARIN n’est utilisée que par Danièle MARIN qui s’engage à ne pas vendre, louer, partager ou divulguer vos données nominatives à des tiers. Son traitement est sécurisé et confiné à un réseau interne.

                Les informations recueillies sur ce site font l’objet d’un traitement informatique destiné aux activités commerciales et marketing de l’établissement.

                ​
                <h3>Traitement, stockage et sécurité de vos données personnelles</h3>

                L’ensemble de vos données personnelles sont traitées, stockées et utilisées avec toute la sécurité nécessaire par Danièle MARIN via un serveur privé basé en France.

                Ces données peuvent, en fonction de vos actions sur notre site, être également stockées, gérées et utilisées par des outils tiers, nécessaires au bon fonctionnement de nos activités.

                Aussi, nous nous assurons que ces outils respectent les normes fixées par le nouveau Règlement Général sur la Protection des Données, en vigueur depuis le 25 mai 2018 au sein de l’Union Européenne.
            </p>


            <h3>Outils tiers utilisés :</h3>


            <p>En fonction de vos actions / demandes sur notre site, deux outils tiers sont susceptibles de collecter, stocker, gérer et utiliser vos données.
            </p>

            <h3>Wordpress :</h3>

            <p>Wordpress est l’outil d’e-mail marketing et transactionnel que nous utilisons, car en parfaite conformité avec le RGPD, conformément à la politique de confidentialité et de sécurité de l’éditeur de cet outil.
            </p>

            <h3>Newsletters et e-mail marketing :</h3>

            <p>L’ensemble de nos formulaires vous permettent de décider si vous souhaitez ou non recevoir : <br>

                Les newsletters d’une part <br>

                Les offres commerciales et promotionnelles d’autre part
            </p>

            <p>
                Vous pouvez décider de recevoir ou non ces différents types d’e-mail, conformément au RGPD. Sans ce consentement explicite de votre part, et dans le cas où vous avez complété l’un de nos formulaires, Danièle MARIN s’engage à ne vous envoyer que les e-mails nécessaire au bon traitement de votre demande (e-mails de double opt-in pour confirmer votre adresse e-mail, etc).<br>

                Ce consentement initial donné par vous-même peut être révoqué à n’importe quel moment. En effet, si vous ne souhaitez plus recevoir d’emails de la part de Danièle MARIN, il vous suffit de cliquer sur les liens de désabonnement prévus à cet effet et présents dans absolument tous nos e-mails, que ce soient ceux adressés à titres marketing ou transactionnel.

                Vous pouvez également nous contacter sur contact@realliances.fr pour nous signaler ce souhait de révocation).

                <h3>Cookies et code de tracking</h3>

                <p>Dans certains cas, Danièle MARIN pourra être amené à utiliser des cookies, ainsi que le code de tracking de certains outils.

                    À noter qu’un cookie aussi nommé « traceur » ou « témoin de connexion » est l’équivalent d’un petit fichier texte stocké sur le terminal de l’internaute et contenant des informations relatives à sa navigation sur Internet : <br>
                    pages consultées, <br>
                    date et heure de consultation, <br>
                    préférences quant aux paramètres des sites visités, <br>
                    reconnaissance de sa présence sur des réseaux sociaux à travers les boutons de partage, etc.<br>

                    L’utilisateur sera dans ce cas informé par un message/une fenêtre pop-up, dès sa première navigation, que le site utilise des cookies. Et ce : <br>

                    pour lui proposer des informations adaptées à ses centres d’intérêts <br>

                    pour lui faciliter la navigation <br> et lui éviter des affichages en double ainsi que pour mesurer l’audience du site.
                </p>

                <p>
                    Le site www.RealliAnceS.com s’engage à ce que ces cookies ne permettent pas l’identification personnelle de l’utilisateur, ni toute utilisation de ses données personnelles par des tiers.

                    A noter que la durée de conservation de ces « cookies » dans l’ordinateur de l’utilisateur ne peut excéder un mois. Le consentement de l’utilisateur concernant l’utilisation des cookies par le site étant quant à lui valable 13 mois maximum, conformément à l’article 32-II de la loi du 6 janvier 1978, modifié par l’ordonnance n°2011-1012 du 24 août 2011, l’utilisateur devra, une fois ce délai passé, à nouveau réitérer ou non son consentement.

                    L’utilisateur peut, s’il le souhaite, s’opposer à la mise en place de cookies.<br> Pour cela, ce dernier peut <br>

                    soit refuser l’utilisation des cookies lors de sa première connexion sur le site en cliquant sur le bandeau prévu à cet effet, auquel cas il ne nous sera plus possible alors de lui offrir toutes les fonctionnalités de notre site, voire d’en rendre inaccessibles certaines parties,<br>

                    soit configurer son navigateur Internet via les paramètres adéquats (désactivation des cookies, choix d’une navigation sécurisée privée, mise en place d’une « confidentialité haute », etc.).<br>

                    La configuration de chaque navigateur étant différente, la procédure permettant de modifier ses souhaits en matière de cookies est décrite dans le menu d’aide dudit navigateur. Ces informations sont également disponibles sur le site du cnil ( http://www.cnil.fr/vos-droits/vos-traces/les-cookies/), tout comme sur la documentation liée à chaque navigateur :<br>

                    Chrome : https://support.google.com/chrome/answer/95647?hl=fr&ref_topic=14666 <br>

                    Internet Explorer : http://windows.microsoft.com/fr-fr/windows7/block-enable-or-allow-cookies <br>

                    Firefox : http://support.mozilla.org/fr/kb/activer-desactiver-cookies?redirectlocale=en-US&redirectslug=Enabling+and+disabling+cookies <br>

                    Safari : http://support.apple.com/kb/HT1677?viewlocale=fr_FR&locale=fr_FR <br>

                    Opéra : http://www.opera.com/help/tutorials/security/cookies/ <br>
                </p>


                <h3>Modification de la Politique de Confidentialité</h3>


                <p>Danièle MARIN se réserve le droit de modifier la présente Politique de confidentialité à tout moment, notamment en application des changements apportés aux lois et réglementations en vigueur.

                    D’autre part, en cas de modification même minime, nous nous engageons à vous en informer par e-mail ou via notre site internet, 30 jours au moins (dans la mesure du possible) avant l’application de ces modifications.

                    Nous vous conseillons également de vérifier cette page régulièrement afin de rester informé, en temps réel, de nos procédures et règles relatives à vos données personnelles.
                </p>

                <h3>Droit d’accès, de rectification et droit à l’oubli</h3>

                <p>Conformément au RGPD et à la loi “informatique et liberté” n°78-17 du 6 janvier 1978, vous disposez d’un droit d’accès, de modification, de rectification et de suppression de vos données personnelles détenues par Danièle MARIN.

                    Vous pouvez exercer ce doit à n’importe quel moment sur simple demande en nous contactant par e-mail sur contact@realliances.fr, en joignant dans les deux cas une copie de votre carte d’identité.<br>

                    realliances@free.fr <br>

                    06 16 77 54 94.

                </p>
        </div>
        <div class="col-2 mb-5 align-self-end">
            <img class="coquelicot" src="<?php echo get_stylesheet_directory_uri(); ?>/image/coquelicot.jpeg" alt="">
        </div>
    </div>
</div>
<?php get_footer(); ?>