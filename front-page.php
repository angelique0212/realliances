<?php get_header(); ?>
<style>
    body {
        background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/image/tree-2521868_1920.jpg");
        background-attachment: fixed;
    }
    .row{background-color: white;
            opacity: 0.9;}
</style>


<div class="container">
  
    <div class="row mt-5 mb-5 p-2">
        <div class="col-12 mt-4">
            <h2 class="text-center">Bienvenue chez</h2>
            <h1 class="titre text-center">Danièle Marin</h1>
            <h3 class="text-center">Sophro-Analyste - Thérapeute certifiée</h3>


            <p class="mt-5 text-justify">Si vous êtes arrivé(e) sur cette page, c’est que vous êtes en quête d’une aide pour surmonter un problème et développer de nouvelles ressources afin d’y parvenir.
            </p>
            <p class="text-justify">Bravo ! Vous avez fait ainsi le premier pas vers le mieux-être auquel vous aspirez. </p>
            <p class="text-justify">Avec<b> RéalliAnceS</b> s’ouvre pour vous la porte d’un accompagnement sur mesure, fondé sur
                l’alliance de thérapies brèves, orientées solutions, dont l’objectif est de vous permettre de (re)trouver
                rapidement votre véritable essence de vie. </p>
            <p class="text-justify">J’ai placé au centre de cet accompagnement la <b style="color: rgba(232,11,0,1);">Sophro-Analyse des Mémoires Prénatales,
                    de la Naissance et de l’Enfance</b>, fantastique méthode thérapeutique qui redonne son sens à l’existence.
                En dialogue avec elle, au cours du processus,<b style="color: rgba(232,11,0,1);"> les Constellations Familiales et Systémiques</b> en individuel
                apportent leur éclairage transgénérationnel et des modes de résolution interrelationnels très puissants,
                et le <b style="color: rgba(232,11,0,1);">Coaching de vie </b> une approche concrète, pas à pas, du quotidien. </p>
            <p class="text-justify">Une plongée dans les mémoires qui ont façonné la personne que vous êtes, des prises de
                conscience qui changeront à jamais votre manière de vous considérer et de vivre vos relations personnelles,
                et un nouveau désir d’aller vers votre avenir comme vers un espace de liberté à construire en harmonie avec
                les aspirations de votre être profond, voici le passionnant voyage qui vous est proposé. </p>
            <p class="text-justify">Ceci dans le respect de votre individualité, le non-jugement, l’ouverture et la bienveillance. </p>
        </div>

    </div>
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 col-xl-2">
            <img class="m-2 enfants_eau" src="<?php echo get_stylesheet_directory_uri(); ?>/image/water-nature-outdoor-people-girl-play-707785-pxhere.com.jpg" alt="">
        </div>
        <div class="col-xl-9 col-lg-12">
            <h5 style="font-size:20px; color:#03bbf7" class="enfant font-weight-bold pb-3">« L’avenir… tu n’as pas à le prévoir, mais à le permettre. » <br>(Saint-Exupéry)</h5>
        </div>
    </div>


    <div class="row mt-5 mb-5">
        <div class="col-12 mt-3">
            <h2 class="titre text-center" style="font-weight:bold;font-size:35px;">Qui suis-je ?</h2>
        </div>
        <div class="col-lg-12 col-xl-6 mt-5 align-self-center">
            <h4 class="font-weight-bold">Tout comme vous, je suis le produit de ce que j’ai vécu et… transformé. </h4><br>
            <p class="text-justify">
                D’abord professeur de lettres puis rédactrice pour l’édition et auteure, j’ai rencontré la Sophro-Analyse<sup>1</sup> à un
                moment déterminant de mon existence. Cette rencontre a libéré ma vie. A tel point que j’ai ressenti le besoin
                impérieux d’en faire mon nouveau métier afin d’aider d’autres personnes à mieux comprendre et transformer ce
                qu’elles ne perçoivent souvent que comme des difficultés et des empêchements… là où ne sont, en réalité,
                qu’obstacles à dépasser et opportunités de découvrir en profondeur l’être singulier, riche de capacités
                 ignorées, qu’elles ont toujours été.
                Afin de mieux les accompagner sur ce passionnant chemin vers la renaissance, je me suis également formée
                à d’autres disciplines, complémentaires :<b> la psychopathologie, les Constellations Familiales<sup>2</sup>,
                    le Coaching de vie<sup>3</sup>, l’hypnose<sup>4</sup>, la P.N.L. (Programmation Neuro-Linguistique), le Reiki Usui.</b> </p>
                   <li style="font-size:10px;"> 1. Formée par Christine Louveau, Centre Emergence Harmonique, Paris. <br></li>
                   <li style="font-size:10px;"> 2. Centre Emergence Harmonique, Paris.<br></li>
                   <li style="font-size:10px;">  3. Lunion Formation, Paris.<br></li>
                   <li style="font-size:10px;">  4. Ecole Centrale d’Hypnose, Paris.</li>
        </div>
        <div class="col-lg-12 col-xl-6 mt-5 mb-5 d-flex justify-content-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/Montage-Dan-PNG-8.png" alt="" width="">
        </div>
    </div>
    <div class="row p-3">


        <div class=" carte col-6">
            <div class="card mb-3 border-0" style="max-width: 18rem;">
                <div class="card-body">
                    <img class="bougie" src="<?php echo get_stylesheet_directory_uri(); ?>/image/zen.jpg" alt="" width="100%" height="100%">
                   
                    <a style="text-decoration:none;text-center;"class="btn btn-primary mb-2 mt-2" href="<?php
                                $link = get_field('coaching');
                                if ($link) :
                                  $link_url = $link['url'];
                                  $link_title = $link['title'];
                                  $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
     <?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">EN SAVOIR PLUS</a>
      <?php endif; ?> SAVOIR PLUS</a>
                </div>
            </div>
        </div>

        <div class="carte col-6">
            <div class="card mb-3 border-0" style="max-width: 18rem;">
                <div class="card-body">

                <?php the_content(); ?>
                </div>
            </div>
        </div>

    </div>
    

    <div class="row mt-5 mb-5">
        <div class="colonne-cabinet col-lg-12 col-xl-4 p-5">
            <h3 class="text-center"><i class="fas fa-house-user"></i> Le cabinet</h3>
            <p class="text-center">- Paris-Centre (Métro Châtelet)<br>- Reçoit aussi à Fontainebleau</p>


        </div>
        <div class="colonne-horaire col-lg-12 col-xl-4 p-5">
            <h3 class="text-center"><i class="far fa-clock"></i> Les horaires</h3>
            <p class="text-center">Du lundi au vendredi <br>9h-19h</p>
            <p class="text-center">Un samedi sur deux : <br>9h-13h </p>
        </div>
        <div class="colonne-contact col-lg-12 col-xl-4 p-5">
            <h3 class="text-center"><i class="fas fa-phone-square-alt"></i> Me contacter</h3>
            <p class="text-center">06 16 77 54 94<br>realliances@free.fr</p>
        </div>
    </div>
</div>


<div class=" mt-5 mb-5 ">
        <div id="carouselExampleControls " class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10598.627791580215!2d2.811749191497447!3d48.38632586808946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47ef5e4a55c3e8f3%3A0x40b82c3688c4c80!2sSaint-Mamm%C3%A8s!5e0!3m2!1sfr!2sfr!4v1599220894359!5m2!1sfr!2sfr" width="300%" height="450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="..." alt="Second slide">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10500.860347289816!2d2.348105141719029!3d48.85410878732598!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e671fd22a8c4a3%3A0x50b82c368941a80!2s4e%20Arrondissement%2C%2075004%20Paris!5e0!3m2!1sfr!2sfr!4v1602684569625!5m2!1sfr!2sfr" width="300%" height="450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
    </div>
   
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>    


<?php get_footer(); ?>