<?php get_header(); ?>


<style type="text/css">
    #banniere {
        height: 300px;
        width: 100%;
    }

    #ancre {
        margin-left: 95%;
        margin-top: 550px;
    }
</style>

<div id="ancre">
    <a href="#"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path id="couleurchevron" d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z" />
        </svg></a>
</div>
<div class="container-fluid">
    <h1 class="titre text-center mt-5 mb-5"> Qu’est-ce que la Sophro-Analyse ?  </h1>

    <div class="row">
        <div id="banniere">
            <img class="bougie" src="<?php echo get_stylesheet_directory_uri(); ?>/image/feeling-1410008_1920.jpg" alt="" width="100%" height="100%">
        </div>
    </div>
</div>
<div class="container">

    <div class="row mt-3 mb-5">

        <div class="col-lg-12 col-xl-6">
            <p class="mt-4 text-justyfy">
                <b style="color: rgba(232,11,0,1);"> Une méthode psychothérapeutique unique et performante</b> qui,
                fondée sur les acquis du décodage des mémoires prénatales introduit par le Dr Claude Imbert dans les années 1990,
                a été développée par Christine Louveau comme une approche restructuratrice globale.<br>
                Fruit de plus de 15 ans de pratique,
                cette méthode permet de libérer tous les scénarios de souffrance qui dérivent des croyances et des mécanismes de survie
                mis en place lors de confrontations avec des situations émotionnelles difficiles, à des stades précoces de notre
                développement, avant, pendant ou après la naissance.<br><br>
                <b style="color: rgba(232,11,0,1);"> Une thérapie brève</b> qui conduit en une vingtaine de séances au déracinement durable
                des croyances limitantes
                à l’origine de nos comportements problématiques et offre ainsi un accès direct à une véritable renaissance à soi-même.<br>
                <img class="bougie" style="margin-left:200px" src="<?php echo get_stylesheet_directory_uri(); ?>/image/LogoÂmeauxcommandes.png" alt="" width="30%" height="30%"><br>


                <b style="color: rgba(232,11,0,1);"> Un processus sécure</b> où tout phénomène inductif est proscrit et qui laisse votre sagesse
                intérieure aux commandes,
                dans le respect des capacités psycho-émotionnelles qui vous sont propres.<br><br>

            </p>
        </div>
        <div class="col-lg-12 col-xl-6">
            <p class="mt-4 text-justify">

                <b style="color: rgba(232,11,0,1);"> Un parcours valorisant</b> dont chaque étape (séance), en soi libératrice, vous propulse
                plus avant vers une autonomie
                radieuse par laquelle vous activez votre propre pouvoir de résilience pour devenir créateur conscient et responsable de votre vie.<br><br>
                <b style="color: rgba(232,11,0,1);"> Une approche holistique </b> qui s’adresse à toutes les dimensions de la personne
                (psycho-émotionnelle, corporelle et spirituelle) pour lui restituer sa place au cœur du vivant.<br><br>
                <?php
                                $link = get_field('chartre');
                                if ($link) :
                                  $link_url = $link['url'];
                                  $link_title = $link['title'];
                                  $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                Ma pratique respecte la <a style="color:black; " href="<?php echo esc_url($link_url); ?>"> <b>Charte de Sophro-Analyse</b><?php endif; ?></a> , laquelle vous garantit un cadre sécurisé, une totale confidentialité et le respect de votre libre-arbitre. <br><br>
                <b>Que n’est pas la Sophro-Analyse des mémoires prénatales? </b><br><br>
                La <b style="color: rgba(232,11,0,1);"> Sophro-Analyse des mémoires prénatales, de la naissance et de l’enfance </b> obéit à la méthode « Votre Âme aux Commandes© » et ne doit pas être confondue avec la sophro-analyse pratiquée sous d’autres formes, la sophrologie ou le rebirth.
            </p>
        </div>

    </div>
    <h2 class="titre text-center mb-4">Comment se passe une séance ? </h2>

    <div class="col-12">
        <div style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/image/Photocabinet.jpg);background-repeat:no-repeat;width:100%;background-size:cover">
            <p class="opacite">
                        VOus êtes en position assise, dans un fauteuil de relaxation.
                        Nous commençons par dialoguer sur les raisons qui vous ont conduit(e) à prendre rendez-vous (si c’est votre première séance) ou des événements et impressions depuis la séance précédente,
                        si vous êtes déjà en cours de thérapie.
                        Nous déterminons ensemble au besoin un objectif de séance.
                        Je vous guide ensuite en état de simple relaxation consciente afin d’apaiser chez vous toute agitation mentale.
                        Par questionnement, je vous invite à remonter, en douceur et sécurité, à la racine de votre problématique actuelle pour <b style="color: black">libérer les mémoires émotionnelles </b>associées,
                        <b style="color:black;">revisiter les interprétations </b> que vous vous étiez faites alors de l’événement, <b style="color: black;">débusquer les croyances et les stratégies
                            limitantes </b> mises en place sous le coup de l’émotion et accéder à une <b style="color: black;">vision élargie de la situation </b>vécue dans la conscience du sens qu’elle a pu revêtir dans votre évolution.
                        L’apaisement et, souvent, la joie authentique qui résultent de ce processus d’intégration vous placent désormais en mesure d’installer durablement
                        en vous de nouvelles énergies vitales. Car vous réinformez instantanément vos
                        cellules pour un changement en profondeur - qui ne tarde pas à se manifester dans votre quotidien, où s’exprime tout votre potentiel libéré.
                        Au terme de ce parcours de quelque 50 minutes, vous êtes invité à revenir à un court échange final en face à face.
            </p>
        </div>
        <p class="mt-4 text-justify">Chaque séance est un parcours hautement sécurisé, dirigé par votre seule sagesse
            intérieure, qui connaît à la fois vos limites et ce que vous avez besoin de libérer.
            Le praticien lui est un partenaire, un facilitateur avisé du processus à l’œuvre. </p>
    </div>
</div>

<div class="container-fluid">
    <h2 class="titre text-center mt-3 mt-5 mb-n4">Pourquoi choisir cet accompagnement ?</h2>

    <div class="row mt-5">
        <div class="col-lg-12 col-xl-4 p-3">
            <p class="text-justify p-3"><b>La liste est quasi illimitée des raisons qui pourraient vous conduire
                    jusqu’à la Sophro-Analyse des mémoires prénatales :</b>
                <li class= "ml-4">Vous rencontrez sur votre chemin des blocages ou des scénarios répétitifs ;</li>
                <li class= "ml-4">Vous ressentez un mal-être indéfinissable et avez du mal à activer votre joie de vivre ou à mettre en œuvre vos ressources ;</li>
                <li class= "ml-4">Vous ne trouvez pas votre place au sein de votre couple, de votre famille, de votre entreprise, de la société ;</li>
                <li class= "ml-4">Vous avez la sensation de tourner en rond ;</li>
                <li class= "ml-4">Vous vivez actuellement une situation douloureuse (maladie, séparation, deuil, crise financière) qui vous paraît insurmontable ;</li>
                <li class= "ml-4">Vous souffrez d'une addiction ou de troubles du comportement alimentaire ; </li>
                <li class= "ml-4">Vous ressassez sans cesse les mêmes souvenirs…</li><br>
            </p>
        </div>
        <div class="align-self-center col-lg-12 col-xl-4 p-3">
            <img class="nature mr-n4 pl-5" src="<?php echo get_stylesheet_directory_uri(); ?>/image/nature-sunlight-spring-child-freedom-fun-1364321-pxhere.com.jpg" alt="" width="100%">
        </div>
        <div class="col-lg-12 col-xl-4 p-3">
            <p class="text-justify p-2"><b>Ainsi, grâce à la Sophro-Analyse, vous pourrez :</b>
                <li class="ml-3">Gagner en autonomie et renoncer à toute dépendance vis-à-vis de thérapies axées
                    sur le seul mental ou tournées vers un soulagement extérieur passager ;</li>
                <li class="ml-3">Apprendre à vous aimer, à respecter et faire respecter vos besoins ;</li>
                <li class="ml-3">Réaliser ce qu’est véritablement aimer l’autre sans l’aliéner ;</li>
                <li class="ml-3">Percevoir l’amour à l’œuvre y compris chez des parents négligents ou maltraitants ;</li>
                <li class="ml-3">Transformer vos peurs viscérales et vos anxiétés en confiance ;</li>
                <li class="ml-3">Libérer votre énergie en sortant une bonne fois pour toutes les émotions tues ou déniées qui vous ont jusqu’ici emprisonné ;</li>
                <li class="ml-3">Retrouver la voie de la créativité (voire débloquer une infertilité non-biologique) en transmutant vos sentiments
                    de culpabilité en responsabilités assumées ;</li>
                <li class="ml-3">Améliorer notablement votre bien-être et votre santé ;</li>
                <li class="ml-3">Saisir le sens de votre chemin de vie et aller vers une compréhension plus fine des enjeux de l’existence ;</li>
                <li class="ml-3">Déployer vos ressources en toute liberté pour vous réaliser.</li>
            </p>
        </div>
    </div>

    <div class="container">

        <p><b style="color: rgba(232,11,0,1);">Aussi révoltant que cela puisse paraître de prime abord, la bonne nouvelle est que la vie vous place face à ce problème comme
                devant une fantastique opportunité d’évoluer vers qui vous êtes vraiment, une chance de vous réaliser pleinement.</b></p>

        <p class="mt-4 mb-4 text-justify"><b>Dans quels cas ne pas choisir cet accompagnement ?</b>  Si motivé par la seule curiosité, vous souhaitez simplement « essayer » .
            En effet, même si les souffrances recontactées en séance peuvent être ressenties comme légères par rapport à ce qu’elles ont été au moment où
            vous les avez vécues, le processus sophro-analytique demande une authentique volonté de se libérer de ses entraves existentielles, une honnêteté
            sans faille envers soi-même et de la persévérance.
            Alors pour vous tester, vous pourriez peut-être vous demander ce que représente votre motivation sur une échelle de 1 à 10.</p>
    </div>


</div>


<?php get_footer(); ?>